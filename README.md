# Setup
### Install scrapy
```
$ pip install scrapy
```

### Create a project  
Syntax: `scrapy startproject prj_name`
```
$ scrapy startproject tabelog_practice
```

### Edit items.py
Edit `prj_name/items.py` and define the fields which you want to scrape. 
Each item needs to inherit `scrapy.Item`.
```
$ vim tabelog_practice/items.py
```

### Create a crawler  
Syntax: `scrapy genspider spider_name domain.com`  
```
$ scrapy genspider tabelog_spider https://tabelog.com/
```
You can find your crawler as `prj_name/spiders/spider_name.py`. 
Edit it so that you can crawl and scrape what you want.

# Run a crawler
Syntax: `scrapy crawl spider_name --output=output.json --logfile=log.txt`
```
$ scrapy crawl tabelog_spider --output=result.json --logfile=hoge.log
```

# Reference
* Scrapy: https://speakerdeck.com/amacbee/pythondezuo-ruwebkuroraru-men
* Crawl tabelog and create a ramen map (a specific case using scrapy and Django): https://qiita.com/yuuki_1204_/items/e64488759454ffe92ab6
