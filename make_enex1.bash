#!/bin/bash

#1:date
#2:2018/03/22
#3:name
#4:博多 一風堂 上野広小路店
#5:region
#6:上野・浅草・日暮里
#7:longitude
#8:35.706042660566894
#9:genre
#10: ラーメン、つけ麺、餃子
#11:score
#12:3.12
#13:link
#14:https://tabelog.com/tokyo/A1311/A131101/13009788/
#15:latitude
#16:139.77253626780833
#17:station
#18:上野広小路駅 183m
#19:holiday
#20:12月31日は～15：00まで。1月1日のみ休業
#21:review
#22:165,

#less ../gyouza.json | tr -d "{}" | awk 'NF>2' | awk 'NR>=115 && NR<=116' |\
less test.json | awk 'NF>2' | tr -d "{}\"" | sort -t"," -k3,3 -rk6,6 -k9,9 | awk -F', |: ' '
BEGIN{
print "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
print "<!DOCTYPE en-export SYSTEM \"http://xml.evernote.com/pub/evernote-export3.dtd\">"
print "<en-export export-date=\"20180315T150040Z\" application=\"Evernote\" version=\"Evernote Mac 6.10 (454269)\">"
print "<note><title>test_</title>"
print "<content><![CDATA[<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>"
print "<!DOCTYPE en-note SYSTEM \"http://xml.evernote.com/pub/enml2.dtd\">"
print "<en-note>"
}

{
odiv="<div>"; cdiv="</div>"
todo="<en-todo checked=\"false\"/>"
ostyle="<a style=\"font-family: &quot;Helvetica Neue&quot;;\""
cstyle=">"
}

a[$6]==0{
print "*************************************"
print odiv "◼︎" $6 cdiv
print "*************************************"
}

{a[$6]++}
a[$6]!=0{
for(i=1;i<=NF;i=i+2){
  gsub("&", "&amp;", $(i+1))
  key=$i
  val=$(i+1)
  x[key]=val
}

print odiv todo  x["name"]  cdiv
print odiv "最寄駅：" x["station"]  cdiv
print odiv "スコア：" x["score"] cdiv
print odiv "定休日：" x["holiday"] cdiv
print odiv ostyle " href=\"" x["link"] "\"" cstyle x["link"] "</a>" cdiv
print "<div><br/></div>"

}

#{
#odiv="<div>"; cdiv="</div>"
#todo="<en-todo checked=\"false\"/>"
#ostyle="<a style=\"font-family: &quot;Helvetica Neue&quot;;\""
#cstyle=">"
##https://s.tabelog.com/tokyo/A1317/A131706/13004530/\">https://s.tabelog.com/tokyo/A1317/A131706/13004530/</a>
##
#
#for(i=1;i<=NF;i++){
#  gsub("\"", "", $i)
#  gsub("&", "&amp;", $i)
#  split($i,arr,": "); 
#  key=arr[1]; 
#  val=arr[2]; 
#  x[key]=val
#}
##for(j in x)print j,x[j]
#print odiv todo  x["name"]  cdiv
#print odiv x[" score"] cdiv
#print odiv ostyle " href=\"" x[" link"] "\"" cstyle x[" link"] "</a>" cdiv
#print "<div><br/></div>"
#
#}

END{
print "</en-note>"
print "]]></content>"
print "<created>20180203T085511Z</created>"
print "<updated>20180315T081450Z</updated>"
print "<note-attributes><latitude>35.70300888145621</latitude>"
print "<longitude>139.6239256126556</longitude>"
print "<altitude>45.89638519287109</altitude>"
print "<source>mobile.iphone</source>"
print "<reminder-order>0</reminder-order>"
print "</note-attributes>"
print "</note>"
print "</en-export>"


}
'
