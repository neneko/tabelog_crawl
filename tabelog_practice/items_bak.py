# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class TabelogPracticeItem(scrapy.Item):
    # define the fields for your item here like:
    name = scrapy.Field()
    date = scrapy.Field()
    score = scrapy.Field()
    link = scrapy.Field()
    reviews_num = scrapy.Field()
    longitude = scrapy.Field()
    latitude = scrapy.Field()
    pass
