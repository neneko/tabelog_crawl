# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class TabelogPracticeItem(scrapy.Item):
    # define the fields for your item here like:
    date = scrapy.Field()
    name = scrapy.Field()
    score = scrapy.Field()
    review = scrapy.Field()
    region = scrapy.Field()
    station = scrapy.Field()
    genre = scrapy.Field()
    link = scrapy.Field()

    # for parse_child
    longitude = scrapy.Field()
    latitude = scrapy.Field()
    holiday = scrapy.Field()
    openhours = scrapy.Field()
    budget = scrapy.Field()
    seats = scrapy.Field()
    tel = scrapy.Field()
    isReservable = scrapy.Field()
    paymethod = scrapy.Field()
    pass
