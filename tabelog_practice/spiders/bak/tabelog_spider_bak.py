# -*- coding: utf-8 -*-

try:
    from urllib.parse import urlparse, parse_qs
except ImportError:
    from urlparse import urlparse, parse_qs
from datetime import datetime
import pytz
import scrapy
from scrapy.contrib.spiders import CrawlSpider
from bs4 import BeautifulSoup
from ..items import TabelogPracticeItem

# 1ページ辺り何件取得するか(動作確認時は1とかにする)
LIMIT_GET_PER_PAGE = 1

class TabelogSpider(CrawlSpider):

    name = "tabelog_spider"
    allowed_domains = ["tabelog.com"]
    start_urls = (
        'https://tabelog.com/tokyo/A1319/rstLst/ramen/1/?Srt=D&SrtT=rt&sort_mode=1',
    )

    def parse(self, response):
        # 店の情報、店のスコアをリストから抽出。
        soup = BeautifulSoup(response.body, "html.parser")
        #f = open("res.html", "w")
        #f.write(soup.prettify())
        #f.close()
        #print soup.prettify()
        summary_list = soup.find_all("a", class_="cpy-rst-name")
        reviews_num = soup.find_all(
            "em", class_="cpy-review-count", limit=LIMIT_GET_PER_PAGE)
        score_list = soup.find_all(
            "span", class_="list-rst__rating-val", limit=LIMIT_GET_PER_PAGE)
        #print 'revews_num:', reviews_num; 
        #print 'score_list:', score_list; 

        for summary, score, rev_num in zip(summary_list, score_list, reviews_num):
            # 店ごとに必要な情報をTabelogcrawlItemに格納。
            jstnow = pytz.timezone(
                'Asia/Tokyo').localize(datetime.now()).strftime('%Y/%m/%d')
            item = TabelogPracticeItem()
            item['date'] = jstnow
            item['name'] = summary.string
            #print "score", score
            item['score'] = score.string
            #print item["score"]
            item['reviews_num'] = rev_num.string
            href = summary["href"]
            item['link'] = href

            # 店の緯度経度を取得する為、
            # 詳細ページもクローリングしてTabelogcrawlItemに格納。
            request = scrapy.Request(
                href, callback=self.parse_child)
            request.meta["item"] = item
            yield request

        # 次ページ。
        soup = BeautifulSoup(response.body, "html.parser")
        next_page = soup.find(
            'a', class_="c-pagination__arrow--next")
        if next_page:
            href = next_page.get('href')
            yield scrapy.Request(href, callback=self.parse)

    def parse_child(self, response):
        # 店の緯度経度を抽出する。
        soup = BeautifulSoup(response.body, "html.parser")
        g = soup.find("img", class_="js-map-lazyload")
        longitude, latitude = parse_qs(
            urlparse(g["data-original"]).query)["center"][0].split(",")
        item = response.meta["item"]
        item['longitude'] = longitude
        item['latitude'] = latitude
        return item
