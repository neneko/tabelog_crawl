# -*- coding: utf-8 -*-
#import ipdb
try:
    from urllib.parse import urlparse, parse_qs
except ImportError:
    from urlparse import urlparse, parse_qs
from datetime import datetime
import pytz
import scrapy
from scrapy.contrib.spiders import CrawlSpider
from bs4 import BeautifulSoup
#from tabelogcrawl.items import TabelogTestItem
#from tabelog_test.items import TabelogTestItem
from ..items import TabelogPracticeItem
import re
import ipdb

# 1ページ辺り何件取得するか(動作確認時は1とかにする)
LIMIT_GET_PER_PAGE = 20

class TabelogSpider(scrapy.Spider):
    name = 'tabelog_spider'
    allowed_domains = ['tabelog.com']
    start_urls = (
        'https://tabelog.com/tokyo/',
        #'https://tabelog.com/tokyo/rstLst/gyouza/1/?Srt=D&SrtT=rt&sort_mode=1',
        #'https://tabelog.com/tokyo/A1319/rstLst/ramen/1/?Srt=D&SrtT=rt&sort_mode=1',
    )

    def __init__(self, genre='cafe', url=None, *args, **kwargs):
        super(TabelogSpider, self).__init__(*args, **kwargs)
        self.genre = genre
        self.hasSpecificUrl = (url!=None)
        self.url = url
        #if url == None:
        #    self.start_urls = ('https://tabelog.com/tokyo/',)
        #else:
        #    self.start_urls = (url,)

    def parse(self, response):
        # 店の情報、店のスコアをリストから抽出。
        soup = BeautifulSoup(response.body, "html.parser")
        #print soup.prettify()

        # Extract URLs of each region
        regions = self.extract_regions(response)
        if self.hasSpecificUrl:
            href = self.url
            region_url = '/'.join(href.split('/')[:5]) + '/'
            the_region = [r for r in regions if r['href'] == region_url][0]
            region_name = the_region.string
            yield scrapy.Request(href, callback=self.parse_region, meta={'region_name': region_name})
        else:
            for each_region in regions:
                #href = each_region["href"] + 'rstLst/gyouza/?SrtT=rt&Srt=D&sort_mode=1'
                href = each_region["href"] + 'rstLst/%s/?SrtT=rt&Srt=D&sort_mode=1' % self.genre
                yield scrapy.Request(href, callback=self.parse_region, meta={'region_name': each_region.string})

        #raw_input()
        #print request; raw_input()

    def parse_region(self, response):
        #ipdb.set_trace()
        region_name = response.meta.get('region_name')
        print region_name
        soup = BeautifulSoup(response.body, "html.parser")
        summary_list = soup.find_all("a", class_="cpy-rst-name")
        score_list = soup.find_all(
            "span", class_="list-rst__rating-val", limit=LIMIT_GET_PER_PAGE)
        review_count = soup.find_all(
            "em", class_="list-rst__rvw-count-num", limit=LIMIT_GET_PER_PAGE)
        num_restaurant = soup.find_all("span", class_="list-condition__count")
        closest_station = soup.find_all('span', class_='list-rst__area-genre')
        #holiday_list = soup.find_all('span', class_='list-rst__holiday-datatxt')

        #for summary, score, review, station, holiday in zip(summary_list, score_list, review_count, closest_station, holiday_list):
        for summary, score, review, station in zip(summary_list, score_list, review_count, closest_station):
            # 店ごとに必要な情報をTabelogPracticeItemに格納。
            jstnow = pytz.timezone(
                'Asia/Tokyo').localize(datetime.now()).strftime('%Y/%m/%d')
            item = TabelogPracticeItem()
            #pdb.set_trace()
            item['date'] = jstnow
            item['name'] = summary.string
            item['score'] = score.string
            item['review'] = review.string
            item['region'] = region_name
            href = summary["href"]
            item['link'] = href
            if len(station.contents) > 1:
                temp = station.contents[0].strip() + station.contents[1].string
            elif len(station.contents) == 1:
                temp = station.contents[0].strip()
            item['station'] = temp.split("/")[0]
            item['genre'] = temp.split("/")[1]
            #item['holiday'] = holiday.string

            # 店の緯度経度を取得する為、
            # 詳細ページもクローリングしてTabelogTestItemに格納。
            request = scrapy.Request(
                href, callback=self.parse_child)
            request.meta["item"] = item
            yield request

        # 次ページ。
        soup = BeautifulSoup(response.body, "html.parser")
        #print soup.prettify()
        #f = open("hoge.html", 'w')
        #f.write(soup.prettify().encoding('utf-8'))
        #f.close()
        #next_page = soup.find(
        #    'a', class_="page-move__target--next")
        next_page = soup.find(
            'a', class_="c-pagination__arrow c-pagination__arrow--next")
        if next_page:
            href = next_page.get('href')
            yield scrapy.Request(href, callback=self.parse_region, meta={'region_name': region_name})

    def extract_regions(self, response):
        soup = BeautifulSoup(response.body, "html.parser")
        hoge = [h for s in soup.find_all("span", class_="list-sidebar__recommend-item")
                  for h in s.find_all("a", class_="list-sidebar__recommend-target") ]
        return hoge

    def parse_child(self, response):
        soup = BeautifulSoup(response.body, "html.parser")
        for br in soup.find_all('br'):
            br.replace_with('\n' + br.text)

        # 店の緯度経度を抽出する。
        g = soup.find("img", class_="js-map-lazyload")
        latitude, longitude = parse_qs(
            urlparse(g["data-original"]).query)["center"][0].split(",")
        item = response.meta["item"]
        item['longitude'] = longitude
        item['latitude'] = latitude

        tel = soup.find("strong", class_='rstinfo-table__tel-num')
        item['tel'] = tel.string

        item['isReservable'] = self.extract_misc(soup, tag='th', nested_tag = 'p', string='予約可否')
        item['holiday']      = self.extract_misc(soup, tag='th', nested_tag = 'p', string='定休日')
        item['openhours']    = self.extract_misc(soup, tag='th', nested_tag = 'p', string='営業時間')
        item['seats']        = self.extract_misc(soup, tag='th', nested_tag = 'p', string='席数')
        item['paymethod']    = self.extract_misc(soup, tag='th', nested_tag = 'p', string='支払い方法')

        soup.find('small').extract()
        item['budget']    = self.extract_misc(soup, tag='th', nested_tag = 'span', string='予算')

        return item

    def extract_misc(self, soup, tag=None, nested_tag=None, string=None):
        ret_tagged = soup.find(tag, string=string)
        if ret_tagged:
            ret = ret_tagged.find_next_sibling('td').find_all(nested_tag)
            ret = '\n'.join([e.text.strip() for e in ret])
            return ret
        else:
            return ""

